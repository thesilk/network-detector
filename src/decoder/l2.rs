use std::fmt;

pub struct L2 {
    destination: String,
    source: String,
}

impl fmt::Display for L2 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "src: {}, dst: {}", self.source, self.destination)
    }
}

pub fn extract_data(packet_data: &[u8]) -> L2 {
    let dest_nums: Vec<String> = packet_data[..6]
        .iter()
        .map(|n| format!("{:02X}", n))
        .collect();
    let src_nums: Vec<String> = packet_data[6..12]
        .iter()
        .map(|n| format!("{:02X}", n))
        .collect();

    L2 {
        destination: dest_nums.join(":"),
        source: src_nums.join(":"),
    }
}
