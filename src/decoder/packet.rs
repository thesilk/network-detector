use super::l2;
use super::l3;
//pub mod l2;
//pub mod l3;

use std::fmt;

pub struct Packet {
    ether: l2::L2,
    ip: l3::L3,
}

impl fmt::Display for Packet {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Ether: {}, IP: {}", self.ether, self.ip)
    }
}

pub fn get_packet_information(packet_data: &[u8]) -> Packet {
    Packet {
        ether: l2::extract_data(&packet_data),
        ip: l3::extract_data(&packet_data),
    }
}
