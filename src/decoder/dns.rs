extern crate hex;

use std::fmt;
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};
use std::str;

struct Dns {
    request: String,
    domain_name: String,
    blocked: bool,
    domain_ip_address: Option<IpAddr>,
}

impl fmt::Display for Dns {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "type: {} -> domain name: {} -> blocked: {} -> ip_address: {:?}",
            self.request, self.domain_name, self.blocked, self.domain_ip_address
        )
    }
}

pub fn decode(packet_data: &[u8]) {
    let domain_name: String = parse_query_domain_name(&packet_data);
    let dns_type = match hex::encode(&packet_data.get(44..46).unwrap()).as_ref() {
        "0100" => String::from("query"),
        "8180" => String::from("response"),
        _ => String::from(""),
    };

    let mut blocked: bool = false;
    let mut domain_ip_address: Option<IpAddr> = None;
    if dns_type == "response" {
        blocked = is_blocked(&packet_data);
        domain_ip_address = get_ip_address(&packet_data);
    }

    let dns = Dns {
        request: dns_type,
        domain_name,
        blocked,
        domain_ip_address,
    };

    println!("{:#}", dns);
}

fn parse_query_domain_name(packet_data: &[u8]) -> String {
    let mut domain_name = String::from("");
    let mut index: usize = 54;

    loop {
        let char_counter = packet_data[index] as usize;
        if char_counter == 0 {
            break;
        }
        index = index + 1;
        if domain_name.len() > 0 {
            domain_name.push_str(".");
        }
        domain_name.push_str(
            str::from_utf8(&packet_data.get(index..index + char_counter).unwrap()).unwrap(),
        );

        index = index + char_counter;
    }
    domain_name
}

fn is_blocked(packet_data: &[u8]) -> bool {
    let index = get_index_after_domain_name(&packet_data, 54) + 5;

    let blocked: bool = match hex::encode(&packet_data.get(index..index + 2).unwrap()).as_ref() {
        "c00c" => true,
        _ => false,
    };
    blocked
}

fn get_ip_address(packet_data: &[u8]) -> Option<IpAddr> {
    let index = get_index_after_domain_name(&packet_data, 54) + 5;
    let mut ip_address: Option<IpAddr> = None;

    if is_blocked(&packet_data) {
        ip_address = match hex::encode(&packet_data.get(index + 2..index + 4).unwrap()).as_str() {
            "0001" => Some(IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0))),
            "001c" => Some(IpAddr::V6(Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 0))),
            _ => panic!("couldn't find blocked ip address"),
        };
    } else {
        let type_index = get_index_after_domain_name(&packet_data, index) + 1;

        if hex::encode(&packet_data.get(type_index..type_index + 2).unwrap()).as_str() == "0001" {
            let ipv4_a: u8 = packet_data[type_index + 10];
            let ipv4_b: u8 = packet_data[type_index + 11];
            let ipv4_c: u8 = packet_data[type_index + 12];
            let ipv4_d: u8 = packet_data[type_index + 13];
            ip_address = Some(IpAddr::V4(Ipv4Addr::new(ipv4_a, ipv4_b, ipv4_c, ipv4_d)));
        } else if hex::encode(&packet_data.get(type_index..type_index + 2).unwrap()).as_str()
            == "001c"
        {
            let ipv6_a: u16 =
                (packet_data[type_index + 10] as u16 * 256) + packet_data[type_index + 11] as u16;
            let ipv6_b: u16 =
                (packet_data[type_index + 12] as u16 * 256) + packet_data[type_index + 13] as u16;
            let ipv6_c: u16 =
                (packet_data[type_index + 14] as u16 * 256) + packet_data[type_index + 15] as u16;
            let ipv6_d: u16 =
                (packet_data[type_index + 16] as u16 * 256) + packet_data[type_index + 17] as u16;
            let ipv6_e: u16 =
                (packet_data[type_index + 18] as u16 * 256) + packet_data[type_index + 19] as u16;
            let ipv6_f: u16 =
                (packet_data[type_index + 20] as u16 * 256) + packet_data[type_index + 21] as u16;
            let ipv6_g: u16 =
                (packet_data[type_index + 22] as u16 * 256) + packet_data[type_index + 23] as u16;
            let ipv6_h: u16 =
                (packet_data[type_index + 24] as u16 * 256) + packet_data[type_index + 25] as u16;
            ip_address = Some(IpAddr::V6(Ipv6Addr::new(
                ipv6_a, ipv6_b, ipv6_c, ipv6_d, ipv6_e, ipv6_f, ipv6_g, ipv6_h,
            )));
        }
    }
    ip_address
}

fn get_index_after_domain_name(packet_data: &[u8], idx: usize) -> usize {
    let mut index: usize = idx;
    loop {
        let char_counter = packet_data[index] as usize;
        if char_counter == 0 {
            break;
        }
        index += 1;
    }
    index
}
